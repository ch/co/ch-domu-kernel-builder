## NB
## Almost all of this Makefile is executed in a chroot environment.
## See the bottom for the top-level commands (inconsistent, I know).

###################################################################################################################################
#
# NB for ubuntu, we expect KVER to be e.g. 4.4.0-64-generic
# and PVER to be  e.g. 4.4.0-64.85
# https://wiki.ubuntu.com/Kernel/FAQ#Kernel.2FFAQ.2FGeneralVersionMeaning.What_does_a_specific_Ubuntu_kernel_version_number_mean.3F
#
###################################################################################################################################


help:
	@echo You probably want to make build\; make build\; make upload

################################################
# Packaging version
################################################
CHVER:=ch1

#################################################
## Distributions we support
## when adding a new Ubuntu, consider setting KPKG_UBUNTU_$DIST later on...
#################################################
DISTS:=
DISTS+=bionic
DISTS+=focal
DISTS+=jammy


#################################################
## Settings for building the domu-modules package
#################################################

# Directories to exclude 
XDIRS:=
XDIRS+=*/fs/nls

# Space-separated list of directories to include
DIRS:=
DIRS+=fs
DIRS+=crypto
DIRS+=kernel/lib
DIRS+=kernel/net/802*
DIRS+=kernel/net/bridge
DIRS+=kernel/net/core
DIRS+=kernel/net/ipv*
DIRS+=kernel/net/llc
DIRS+=kernel/net/mac80211
DIRS+=kernel/net/netfilter
DIRS+=kernel/net/sunrpc
DIRS+=kernel/drivers/block

# Space-separated list of files to include
FILES:=
FILES+=*xen*.ko
FILES+=*tun.ko
FILES+=*veth.ko
FILES+=*macvlan.ko
FILES+=modules.order
FILES+=modules.builtin

## Our custom postinst file
define POSTINST
#!/bin/bash
/sbin/depmod $(KVER)
endef
export POSTINST

.SECONDARY: $(SECL) $(TDIR) kernel-$(DIST).deb

TDIR:=$(shell mktemp -d)

#modules.deb: PVER=$(shell grep ^Version $(TDIR)/DEBIAN/control | sed s/.*:.//)
modules.deb: PVER=$(shell dpkg -s linux-image-$(KVER) | grep Version | sed s/.*:.//)
modules.deb: DDIR:=$(shell mktemp -d)
modules.deb: FXDIR:=$(foreach X,$(XDIRS),-wholename '$(X)' -o)
modules.deb: FDDIR:=$(foreach D,$(DIRS),-wholename '*/$(D)/*' -o)
modules.deb: FFILE:=$(foreach F,$(FILES),-name '$(F)' -o)
modules.deb: 
	rm -f modules.deb
	# Copy the bits we want
	mkdir -p $(DDIR)/DEBIAN
	(cd / && find ./lib/modules/$(KVER) -type f \( $(FXDIR) -false \) -prune -o \( $(FDDIR) $(FFILE) -false \) | tar -cf - -T - ) | (cd $(DDIR) && tar -xvf - )
	# Tweak the DEBIAN part
	dpkg-deb -I /var/cache/apt/archives/linux-image-$(KVER)_$(PVER)_amd64.deb control >$(DDIR)/DEBIAN/control
	@echo "$$POSTINST" >$(DDIR)/DEBIAN/postinst
	@chmod 755 $(DDIR)/DEBIAN/postinst
	sed -i 's/Maintainer:.*/Maintainer: support@ch.cam.ac.uk/' $(DDIR)/DEBIAN/control
	sed -i 's/Provides: /Provides: linux-image-$(PVER), ch-domu-modules, /' $(DDIR)/DEBIAN/control
	sed -i 's/Package: linux-image-.*/Package: ch-domu-mods-$(DIST)/' $(DDIR)/DEBIAN/control
	# The linux-image deb now Depends on linux-modules in Xenial, which ends up conflicting
	# with our modules package. Let's hard-code the Depends: for now until we find this problematic
	sed -i 's/Depends:.*/Depends: kmod/' $(DDIR)/DEBIAN/control
	# Sometimes we end up with a Recommends of grub, os-prober, etc. We don't want these; our modules.deb
	# is really just a tarball of .ko files
	sed -i '/^Recommends/d' $(DDIR)/DEBIAN/control
	sed -i 's/^Description: /Description: Chem-DomU/' $(DDIR)/DEBIAN/control
	sed -i 's/^Version: .*/&-$(CHVER)/' $(DDIR)/DEBIAN/control
	(cd $(DDIR) && find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sums)
	dpkg-deb -Zxz -b $(DDIR) .
	ln -s ./ch-domu-mods-$(DIST)_$(PVER)-$(CHVER)_amd64.deb modules.deb

#################################################
## A DEB pkg containing kernels for domu hosts
#################################################

# To sort out kernels
TYPE:=$(shell (grep -q ubuntu.com /etc/apt/sources.list && echo ubuntu) || (grep -q debian.org /etc/apt/sources.list && echo debian) || echo unknown)
DIST:=$(shell grep -v ^\# /etc/apt/sources.list | head -1 | awk ' { print $$3 } ' )
KVER:=$(shell (ls -1t /boot/vmlinuz-* 2>/dev/null || echo ) | head -1 | sed s/.*vmlinuz-// )
PKGN:=ch-domu-kernel-$(DIST)
KDIR:=$(PKGN)/usr/lib/ch-domu-kernels/$(DIST)
SECL:=/etc/apt/sources.list.d/security.list

#KPKG_ubuntu_bionic:=linux-image-generic
#KPKG_ubuntu:=$(KPKG_ubuntu_$(DIST))
KPKG_ubuntu:=linux-image-generic

KPKG_debian:=$(shell apt-cache depends linux-image-amd64 | grep Depends | cut -d ':' -f 2)
KPKG=$(KPKG_$(TYPE))

VERS:=$(shell apt-cache policy $(KPKG) | awk ' $$1 == "***" { print $$2 } ' )

define CONTROL_TEMPLATE
Package: $(PKGN)
Version: $(VERS)-$(CHVER)
Section: kernel
Priority: optional
Maintainer: Chemistry Support <support@ch.cam.ac.uk>
Architecture: amd64
Description: Provides domu kernels for $(DIST) machines (to be installed on Dom0)
 DomU kernel for $(DIST) VMs; install this on Dom0
endef
export CONTROL_TEMPLATE

define UBUNTU_SECURITY
deb http://archive.ubuntu.com/ubuntu $(DIST)-updates main
deb http://archive.ubuntu.com/ubuntu $(DIST)-security main
endef
export UBUNTU_SECURITY

define DEBIAN_SECURITY
deb http://security.debian.org/ $(DIST)/updates main
endef
export DEBIAN_SECURITY

security: $(TYPE)-security

$(SECL): $(TYPE)-security

update: security
	@apt-get update -y -qq
	@apt-get upgrade -y -qq
	
kernel: update 
	@apt-get remove grub-common grub-pc grub -y
	@apt-get install $(KPKG) -y

ubuntu-security:
	@echo Adding security list for Ubuntu
	@echo "$$UBUNTU_SECURITY" >$(SECL)

debian-security:
	@echo Adding security list for Debian
	@echo "$$DEBIAN_SECURITY" >$(SECL)

unknown-security:
	@echo Hmm, really ought not to get to this target

kernel.deb: kernel-$(DIST).deb

kernel-$(DIST).deb: VER=$(shell grep Version $(PKGN)/DEBIAN/control  | awk ' { print $$2 } ')
kernel-$(DIST).deb: kernel kclean $(KDIR)/vmlinuz $(KDIR)/initrd.img $(KDIR)/System.map $(KDIR)/config $(PKGN)/DEBIAN/control
	@rm -f $(KDIR)/System.map $(KDIR)/config kernel-$(DIST).deb
	@dpkg-deb -Zxz --build $(PKGN) .
	ln -s ./ch-domu-kernel-$(DIST)_$(VER)_amd64.deb kernel-$(DIST).deb

kclean:
	@rm -f $(KDIR)/* $(PKGN)/DEBIAN/control 

$(PKGN):
	@mkdir ch-domu-kernel-$(DIST) -p

$(PKGN)/DEBIAN/control: ch-domu-kernel-$(DIST)
	@mkdir ch-domu-kernel-$(DIST)/DEBIAN/ -p
	@echo "$$CONTROL_TEMPLATE" >ch-domu-kernel-$(DIST)/DEBIAN/control
	
$(KDIR)/%: $(KDIR)
	@cp /boot/$*-$(KVER) $(KDIR)
	@(cd $(KDIR) && ln -s ./$*-$(KVER) $*)

$(KDIR): ch-domu-kernel-$(DIST)
	@mkdir -p $(KDIR)

##
## These are the targets called in the top level
## 

# Called above the chroots
# Create a chroot environment
%-chroot:
	$(info Creating chroot for $*)
	@mkdir -p chroots/$*
	@chroot chroots/$* true >/dev/null 2>/dev/null || debootstrap --include=make $* chroots/$*

# Chroot and run make on the target
make-chroot-%: CHROOT=$(firstword $(subst -, ,$*))
make-chroot-%: CMD=$(subst $(CHROOT)-,,$*)
make-chroot-%:
	$(info Entering the $(CHROOT) chroot to make $(CMD))
	@umount chroots/$(CHROOT)/proc >/dev/null 2>&1 || true
	@mount -t proc proc chroots/$(CHROOT)/proc
	@cp Makefile chroots/$(CHROOT)/root/Makefile
	@chroot chroots/$(CHROOT) bash -c "cd /root && make $(CMD)"
	@umount chroots/$(CHROOT)/proc
	$(info Leaving the $(CHROOT) chroot after making $(CMD))

# Some distros require /proc to be mounted (to check for PAE on processor)
%-deb: %-chroot 
	@sudo umount chroots/$*/proc || true
	@sudo mount -t proc proc chroots/$*/proc
	@sudo cp Makefile chroots/$*/root/Makefile
	@sudo chroot chroots/$* bash -c "cd /root && make all"
	@sudo cp chroots/$*/root/*.deb ./debs
	@sudo umount chroots/$*/proc

# 

debs: bionic-deb focal-deb jammy-deb

chroots:
	mkdir -p chroots


# Something about this doesn't work with more than one thing in TDISTS
# N.B. TDISTS is just for the dom0s where we want the kernel packages
scp-%: KDEB=$(shell readlink chroots/$*/root/kernel-$*.deb)
scp-%: MDEB=$(shell readlink chroots/$*/root/modules.deb)
scp-%: TDISTS:=
#scp-%: TDISTS+=buster
scp-%: TDISTS+=bullseye
scp-%: 
	scp chroots/$*/root/$(KDEB) chroots/$*/root/$(MDEB) root@downloads.ch.private.cam.ac.uk:/tmp/
	$(foreach TDIST,$(TDISTS),ssh -l root downloads.ch.private.cam.ac.uk "cd /var/www/html/local-debs && make debtodist-$(TDIST) DEB=/tmp/$(KDEB)")
	ssh -l root downloads.ch.private.cam.ac.uk "cd /var/www/html/local-debs && make debtodist-$* DEB=/tmp/$(MDEB)"

%-clean:
	rm -f chroots/$*/root/*.deb

############################################
## High-level makefile targets 
############################################

clean: $(foreach DIST,$(DISTS),$(DIST)-clean)

build: chroots $(foreach DIST,$(DISTS),$(DIST)-chroot) $(foreach DIST,$(DISTS),make-chroot-$(DIST)-update) $(foreach DIST,$(DISTS),make-chroot-$(DIST)-kernel.deb) $(foreach DIST,$(DISTS),make-chroot-$(DIST)-modules.deb)
	$(info All distributions now built)

upload: build $(foreach DIST,$(DISTS),scp-$(DIST))

all: upload


